module.exports = {
    '/api/about': require('./controllers/about.controller'),
    '/api/catalog': require('./controllers/catalog.controller'),
    '/api/contacts': require('./controllers/contacts.controller'),
    '/api/lookbook': require('./controllers/lookbook.controller')
};