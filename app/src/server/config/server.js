var isDevelopment = process.env.NODE_ENV == 'development';

var db = {
    user: 'soberay',
    pswd: 'braveweek2012M',
    host: isDevelopment ? 'localhost' : 'soberay.com.ua'
};

module.exports = {
    secret : 'Soberay',
    database : !isDevelopment ? 'mongodb://' + db.user + ':' + db.pswd + '@' + db.host + '/soberay' : 'mongodb://' + db.host + '/soberay',
    port : 3000
};