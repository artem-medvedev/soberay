(function () {
  "use strict";

  var app = angular.module('app', ['ng-admin', 'angularFileUpload']);

  // API Mapping
  app.config(['RestangularProvider', function (RestangularProvider) {

    // use the custom query parameters function to format the API request correctly
    RestangularProvider.addFullRequestInterceptor(function (element, operation, what, url, headers, params) {
      if (operation === 'getList') {
        // custom pagination params
        if (params._page) {
          var start = (params._page - 1) * params._perPage;
          var end = params._page * params._perPage - 1;
          params.range = "[" + start + "," + end + "]";
          delete params._page;
          delete params._perPage;
        }
        // custom sort params
        if (params._sortField) {
          params.sort = '["' + params._sortField + '","' + params._sortDir + '"]';
          delete params._sortField;
          delete params._sortDir;
        }
        // custom filters
        if (params._filters) {
          params.filter = params._filters;
          delete params._filters;
        }
      }
      return {params: params};
    });

    // RestangularProvider.addFullRequestInterceptor(function (element, operation, what, url, headers, params) {
    //   if (operation == "getList") {
    //     delete params._page;
    //     delete params._perPage;
    //     delete params._sortField;
    //     delete params._sortDir;
    //   }
    //   return {params: params};
    // });

    RestangularProvider.addResponseInterceptor(function (data, operation, what, url, response) {
      if (operation === "getList") {
        var headers = response.headers();
        if (headers['content-range']) {
          response.totalCount = headers['content-range'].split('/').pop();
        }
      }

      return data;
    });
  }]);

  // Admin definition
  app.config(['NgAdminConfigurationProvider', function (NgAdminConfigurationProvider) {
    var nga = NgAdminConfigurationProvider;

    var admin = nga.application('Soberay admin')
      .debug(true)
      .baseApiUrl('http://localhost:3000/api/');

    // define entities
    var about = nga.entity('about')
      .identifier(nga.field('_id'));

    var contacts = nga.entity('contacts')
      .identifier(nga.field('_id'));

    var lookbook = nga.entity('lookbook')
      .identifier(nga.field('_id'));


    // set the application entities
    admin
      .addEntity(about)
      .addEntity(contacts)
      .addEntity(lookbook);


    /*****************************
     * About entity customization *
     *****************************/

    about.listView()
      // .title('About us')
      .description('List of the all items in the catalog')
      // .actions([])
      .batchActions([])
      .listActions(['show', 'edit', 'delete'])
      .fields([
        nga.field('title').isDetailLink(true)
          .label('Title')
          .detailLinkRoute('show')
      ]);

    about.editionView()
      .title('Edit About us')
      .actions(['back'])
      .fields([
        nga.field('title'),
        nga.field('subtitle'),
        nga.field('text', 'text')
      ]);

    about.showView()
      .title('About us')
      .actions(['edit'])
      .fields(about.editionView().fields());

    /*****************************
     * Contacts entity customization *
     *****************************/

    contacts.listView()
      .title('Contacts')
      .description('Contacts')
      .actions([])
      .batchActions([])
      .listActions(['show', 'edit'])
      .fields([
        nga.field('title').isDetailLink(true)
          .label('Title')
          .detailLinkRoute('show')
      ]);

    contacts.editionView()
      .title('Edit Contacts')
      .actions(['back'])
      .fields([
        nga.field('title'),
        nga.field('subtitle'),
        nga.field('town'),
        nga.field('email', 'email'),
        nga.field('tel'),
        nga.field('shops', 'embedded_list')
          .targetFields([
            nga.field('town'),
            nga.field('links', 'embedded_list')
              .targetFields([
                nga.field('name'),
                nga.field('url')
                  .cssClasses('col-md-6')
              ])
          ])
          // .cssClasses('col-sm-10 col-md-10 col-lg-7')
      ]);

    contacts.showView()
      .title('Contacts')
      .actions(['edit'])
      .fields(contacts.editionView().fields());


    /*****************************
     * Lookbook entity           *
     *****************************/

    lookbook.listView()
      .title('Lookbook')
      .description('List of the all photos')
      .listActions(['show', 'delete'])
      .actions(['batch', '<upload-photos/>'])
      .fields([
        nga.field('thumb', 'template')
          .label('Thumb')
          .template('<a href="#/lookbook/show/{{ entry.values._id }}"><img src="http://localhost:3000/{{ entry.values.src }}?dim=200" class="thumb"></a>'),
        nga.field('_id').isDetailLink(true)
          .label('ID'),
        nga.field('Full size')
          .map(function (value, entry) {
            return entry.h + ' * ' + entry.w + ' px';
          })
      ]);

    lookbook.showView()
      .title('Lookbook photo')
      .actions(['delete', 'back'])
      .fields([
        nga.field('')
          .label('')
          .template('<img class="photo-preview" ng-src="http://localhost:7203/{{ entry.values.src }}">')
      ]);

    lookbook.deletionView()
      .title('Delete photo')
      .actions(['back'])
      .fields([
        nga.field('_id')
          .label('TEST')
      ]);


    // customize header
    var customHeaderTemplate =
      '<div class="navbar-header">' +
      '<button type="button" class="navbar-toggle" ng-click="isCollapsed = !isCollapsed">' +
      '<span class="icon-bar"></span>' +
      '<span class="icon-bar"></span>' +
      '<span class="icon-bar"></span>' +
      '</button>' +
      '<a class="navbar-brand" href="#" ng-click="appController.displayHome()">Soberay admin</a>' +
      '</div>' +
      '<p class="navbar-text navbar-right hidden-xs">' +
      '<a href="https://github.com/marmelab/ng-admin/blob/master/examples/blog/config.js"><span class="glyphicon glyphicon-sunglasses"></span>&nbsp;View Source</a>' +
      '</p>';
    admin.header(customHeaderTemplate);

    // customize menu
    admin.menu(nga.menu()
      .addChild(nga.menu(about).icon('<span class="glyphicon glyphicon-info-sign"></span>').link('/about/show/57387e95e17cde4378b10c75'))
      .addChild(nga.menu(contacts).icon('<span class="glyphicon glyphicon-earphone"></span>').link('/contacts/show/57388d8814d76bc2783b591c'))
      .addChild(nga.menu(lookbook).icon('<span class="glyphicon glyphicon-tags"></span>'))
    );

    nga.configure(admin);
  }]);


  // custom 'lookbook' page
  app.directive('uploadPhotos', [function () {
    return {
      restrict: 'E',
      template: '<a class="btn btn-default" ui-sref="upload"><span class="glyphicon glyphicon-plus"></span> <span class="hidden-xs">Upload photos</span></a>'
    };
  }]);

  app.directive('ngThumb', ['$window', function($window) {
    var helper = {
      support: !!($window.FileReader && $window.CanvasRenderingContext2D),
      isFile: function (item) {
        return angular.isObject(item) && item instanceof $window.File;
      }
    };

    return {
      restrict: 'A',
      template: '<canvas/>',
      link: function (scope, element, attributes) {
        if (!helper.support) return;

        var params = scope.$eval(attributes.ngThumb),
          canvas = element.find('canvas'),
          reader = new FileReader();

        if (!helper.isFile(params.file)) {
          return;
        }

        reader.onload = onLoadFile;
        reader.readAsDataURL(params.file);

        function onLoadFile(event) {
          var img = new Image();
          img.onload = onLoadImage;
          img.src = event.target.result;
        }

        function onLoadImage() {
          var width = params.width || this.width / this.height * params.height;
          var height = params.height || this.height / this.width * params.width;
          canvas.attr({width: width, height: height});
          canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
        }
      }
    };
  }]);

  function uploadPhotoController($scope, FileUploader, $location, notification) {
    var uploader = $scope.uploader = new FileUploader({
      url: 'http://localhost:3000/api/lookbook',
      alias: 'lookbook',
      filters: [{
        name: 'imageFilter',
        fn: function(item) {
          var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
          return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
      }]
    });

    uploader.onCompleteAll = function() {
      notification.log('Photo uploaded successfully', {addnCls: 'humane-flatty-success'});
      if (uploader.queue.length == 0) {
        notification.log('Successfully uploaded ' + uploader.queue.length + ' photos', {addnCls: 'humane-flatty-success'});
        $location.path('/lookbook/list');
      }
    };
  }
  uploadPhotoController.$inject = ['$scope', 'FileUploader', '$location', 'notification'];

  app.config(['$stateProvider', function ($stateProvider) {
    $stateProvider.state('upload', {
      parent: 'main',
      url: '/lookbook/upload',
      controller: uploadPhotoController,
      templateUrl: 'views/upload-photo.html'
    });
  }]);

}());
