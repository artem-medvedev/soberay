var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var Links = new Schema({
  name: String,
  url: String
});

var Shops = new Schema({
  town: String,
  links: [Links]
});

var ContactsSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  subtitle: {
    type: String,
    required: true
  },
  town: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  tel: {
    type: String,
    required: true
  },
  shops: [Shops]
});

// Export the model.
module.exports = mongoose.model('Contacts', ContactsSchema);