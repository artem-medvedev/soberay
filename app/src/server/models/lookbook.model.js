var mongoose = require('mongoose');

// Create the MovieSchema.
var LookbookSchema = new mongoose.Schema({
    src: {
        type: String,
        unique: true,
        required: true
    },
    h: {
        type: Number,
        required: true
    },
    w: {
        type: Number,
        required: true
    },
    createdAt: String,
    updatedAt: String
}, {
    collection: 'lookbook'
});

LookbookSchema.pre('save', function(next) {
    var currentDate = new Date();

    this.updatedAt = currentDate;

    if (!this.createdAt) {
        this.createdAt = currentDate;
    }

    next();
});

// Export the model.
module.exports = mongoose.model('Lookbook', LookbookSchema);