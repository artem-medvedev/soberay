var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var AboutSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  subtitle: {
    type: String,
    required: true
  },
  text: {
    type: String,
    required: true
  }
  // contacts: Contacts,
  // about: About
  // {
  //   collection: 'pages'
  // }
}, {
  collection: 'about'
});

// Export the model.
module.exports = mongoose.model('About', AboutSchema );