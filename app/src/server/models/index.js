module.exports = {
  about: require('./about.model'),
  catalog: require('./catalog.model'),
  contacts: require('./contacts.model'),
  lookbook: require('./lookbook.model')
};