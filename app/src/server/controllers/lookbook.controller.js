var restful   = require('node-restful'),
    multer    = require('multer'),
    sizeOf    = require('image-size'),
    four0four = require('../utils/404')(),
    fs        = require('fs');

module.exports = function (app, route) {
  var Lookbook = restful.model('Lookbook',
    app.models.lookbook
  );
  
  Lookbook
    .methods(['get', 'put', 'post', 'delete'])
    .before('post', uploadPhoto)
    .before('delete', deletePhoto);

  function uploadPhoto(req, res, next) {
    var storage = multer.diskStorage({
      destination: function (req, file, cb) {
        cb(null, './src/client/images/lookbook')
      },
      filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1])
      }
    });

    var upload = multer({
      storage: storage
    }).array('lookbook', 12);

    upload(req, res, function (err) {
      if (err) {
        four0four.send404(req, res, err);
      }

      var dimensions = sizeOf(req.files[0].path);
      var photo = new Lookbook({
        src: '/images/lookbook/' + req.files[0].filename,
        h: dimensions.height,
        w: dimensions.width
      });

      photo.save(function (err) {
        if (err) {
          four0four.send404(req, res, err);
        }

        res.json({message: 'Photo added', data: photo});
      });
    });
  }

  function deletePhoto(req, res, next) {
    Lookbook.findByIdAndRemove(req.params.id, function(err, model) {
      if (err) {
        four0four.send404(req, res, err);
      }

      fs.unlink('./src/client' + model.src, function(err, file) {
        if (err) {
          four0four.send404(req, res, err);
        }

        res.json({ message: 'Successfully deleted' });
      });
    });
  }

  Lookbook.register(app, route);

  return function (req, res, next) {
    next();
  };
};