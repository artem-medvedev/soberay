(function () {
    'use strict';

    angular
        .module('app.home')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['logger', '$rootScope'];

    /* @ngInject */
    function HomeController(logger, $rootScope) {
        var vm = this;
        vm.title = 'Home';

        activate();

        ////////////////

        function activate() {
            // spinnerService.hide('loading');
            
            // logger.success('Home loaded!', null);
        }
    }
})();