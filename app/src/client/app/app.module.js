(function() {
    'use strict';

    angular.module('app', [
        /* Shared modules */
        'app.core',
        'app.widgets',

        /* Feature areas */
        'app.layout',
        'app.home',
        'app.lookbook',
        'app.contacts',
        'app.about',
        'app.catalog'
    ]);
})();