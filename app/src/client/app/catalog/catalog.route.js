(function() {
    'use strict';

    angular
        .module('app.catalog')
        .run(appRun);

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    function getStates() {
        return [{
            state: 'catalog',
            config: {
                url: '/catalog',
                templateUrl: 'app/catalog/catalog.html',
                controller: 'CatalogController as vm',
                title: 'Catalog',
                resolve: { /* @ngInject */
                    catalog: function(dataservice) {
                        return dataservice.getPhotos();
                    }
                }
            }
        }];
    }
})();