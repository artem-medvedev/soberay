(function () {
    'use strict';

    angular
        .module('app.catalog')
        .controller('CatalogController', CatalogController);

    /* @ngInject */
    function CatalogController(catalog) {
        var vm = this;
        vm.title = 'CatalogController';

        activate();
        
        ////////////////

        function activate() {
            vm.page = 0;
            vm.shots = [];
            vm.selectedGroup = '';

            // var shotsTmp = angular.copy(vm.shots);
            var shotsTmp = catalog;

            vm.shots = shotsTmp;

            // vm.categories = categories;

            vm.angularGridOptions = {
                gridWidth : 300,
                gutterSize : 20,
                // refreshOnImgLoad : true
                // scrollContainer: '#catalog',
                //infiniteScrollDistance: 50
            };


            vm.showAll = function () {
                vm.shots = shotsTmp;
            };

            vm.showImagesOfType  = function(type){
                vm.shots = shotsTmp.filter(function (obj) {
                    return obj.group == type;
                });
            };
        }
    }
})();