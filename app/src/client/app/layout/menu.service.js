(function () {
    'use strict';

    angular
        .module('app.layout')
        .service('menuService', MenuService);

    /* @ngInject */
    function MenuService($document) {
        var openScope = null, service;
        service = {
            open: open,
            close: close
        };

        return service;

        function open(menuScope) {
            $document.on('keydown', escapeKeyBind);
            openScope = menuScope;
        }

        function close(menuScope) {
            if ( openScope === menuScope ) {
                openScope = null;
                $document.off('keydown', escapeKeyBind);
            }
        }

        function closeMenu() {
            openScope.$apply(function() {
                openScope.isOpen = false;
            });
        }

        function escapeKeyBind(e) {
            if (e.keyCode === 27) {
                console.log('esc');
                closeMenu();
            }
        }
    }
})();