(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('MenuController', MenuController);

    /* @ngInject */
    function MenuController($scope, $attrs, $rootScope, menuService, $state, $timeout) {
        var vm = this,
            delayPromise,
            openClass = 'menu-opened';

        vm.init = function( element ) {
            vm.$element = element;
            vm.toggleElement = element.find('button')[0];
            $scope.isOpen = angular.isDefined($attrs.isOpen) ? $scope.$parent.$eval($attrs.isOpen) : false;

            $rootScope.$on('$stateChangeStart', function (event, toState) {
                if (vm.isOpen()) {
                    event.preventDefault();
                    vm.toggle();
                    delayPromise = $timeout(function () {
                        $state.go(toState);
                    }, 350);
                }
            });

            $rootScope.$on('$stateChangeSuccess', function() {
                if (delayPromise) {
                    $timeout.cancel(delayPromise);
                }
            });
        };

        vm.toggle = function( open ) {
            return $scope.isOpen = arguments.length ? !!open : !$scope.isOpen;
        };

        // Allow other directives to watch status
        vm.isOpen = function() {
            return $scope.isOpen;
        };

        $scope.$watch('isOpen', function( value ) {
            if ( value ) {
                menuService.open( $scope );
                vm.$element.addClass(openClass);
            } else {
                menuService.close( $scope );
                vm.$element.removeClass(openClass);
            }

            $scope.onToggle({ open: !!value });
        });

        // $scope.$on('$locationChangeSuccess', function() {
        //     $scope.isOpen = false;
        // });
    }
})();