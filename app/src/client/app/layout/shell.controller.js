(function() {
    'use strict';

    angular
        .module('app.layout')
        .controller('ShellController', ShellController);

    /* @ngInject */
    function ShellController($document, $rootScope, config, logger, envService) {
        var vm = this;

        vm.title = config.appTitle;

        // vm.menuOpen = false;
        //
        // vm.toggleMenu = function() {
        //     vm.menuOpen = !vm.menuOpen;
        //     console.log(vm.menuOpen);
        // };

        // $rootScope.isAppLoading = false;

        // vm.showSpinner = function(spinner) {
        //     spinner.show();
        // };

        activate();

        function activate() {
            // logger.success(config.appTitle + ' loaded!', null);
            // hideSplash();
        }

        // function hideSplash() {
        //     vm.showSpinner = false;
        //     //Force a 1 second delay so we can see the splash.
        //     // $timeout(function() {
        //     //     vm.showSplash = false;
        //     // }, 1000);
        // }
    }
})();
