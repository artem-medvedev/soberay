(function() {
    'use strict';

    angular
        .module('app.layout')
        .directive('sbMenu', sbMenu);

    /* @ngInject */
    function sbMenu () {
        var directive = {
            templateUrl: 'app/layout/menu.html',
            restrict: 'E',
            controller: 'MenuController as vm',
            replace: true,
            scope: {
                isOpen: '=?',
                onToggle: '&'
            },
            link: link
        };
        return directive;

        function link(scope, element, attrs, MenuController) {
            MenuController.init( element );
        }
    }
})();