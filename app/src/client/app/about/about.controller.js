(function () {
  'use strict';

  angular
    .module('app.about')
    .controller('AboutController', AboutController);

  /* @ngInject */
  function AboutController(dataservice) {
    var vm = this;
    vm.title = 'AboutController';

    dataservice.about().query(function(data) {
      vm.about = data[0];
    });

    // activate();

    ////////////////

    // function activate() {
    //     console.log('123');
    // }
  }

})();

