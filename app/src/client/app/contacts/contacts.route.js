(function () {
  'use strict';

  angular
    .module('app.contacts')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [{
      state: 'contacts',
      config: {
        url: '/contacts',
        templateUrl: 'app/contacts/contacts.html',
        controller: 'ContactsController',
        controllerAs: 'vm',
        title: 'Contacts'
      }
    }];
  }
})();