(function () {
  'use strict';

  angular
    .module('app.contacts')
    .controller('ContactsController', ContactsController);

  /* @ngInject */
  function ContactsController(dataservice) {
    var vm = this;
    vm.contacts = {};

    dataservice.contacts().query(function(data) {
      vm.contacts = data[0];
      
      console.log(vm.contacts);
    });
    
    // activate();

    ////////////////

    // function activate() {
    //     console.log('123');
    // }
  }

})();