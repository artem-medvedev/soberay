(function () {
  'use strict';

  angular
    .module('app.lookbook')
    .run(appRun);

  appRun.$inject = ['routerHelper'];

  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [{
      state: 'lookbook',
      config: {
        url: '/lookbook',
        title: 'Lookbook',
        templateUrl: 'app/lookbook/lookbook.html',
        controller: 'LookbookController',
        controllerAs: 'vm',
        resolve: {
          /* @ngInject */
          lookbook: function (dataservice) {
            return dataservice.getPhotos();
          }
        }
      }
    }
    ];
  }
})();