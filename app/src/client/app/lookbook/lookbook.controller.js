(function () {
  'use strict';

  angular
    .module('app.lookbook')
    .controller('LookbookController', LookbookController);

  /* @ngInject */
  function LookbookController(lookbook) {
    var vm = this;
    vm.title = 'Lookbook';

    vm.photos = lookbook;

    console.log(lookbook);

    vm.angularGridOptions = {
      gridWidth: 300,
      gutterSize: 20,
      scrollContainer: '#lookbook',
      refreshOnImgLoad: false
    };


    // activate();

    ////////////////

    // function activate() {
    //     return getPhotos().then(function () {
    //         logger.info('Activated Lookbook View');
    //     });
    // }
  }
})();