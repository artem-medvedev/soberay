(function() {
    'use strict';

    angular
        .module('app.core', [
            /* Angular modules */
            'ngAnimate', 'ngResource',
            // 'ngSanitize',
            /* Cross-app modules */
            'blocks.diagnostics',
            'blocks.exception',
            'blocks.logger',
            'blocks.router',
            'environment',
            /* 3rd-party modules */
            'ui.router', 'ng-fastclick', 'ngSVGAttributes', 'angular-loading-bar'
        ]);
})();