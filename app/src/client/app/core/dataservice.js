(function () {
  'use strict';

  angular
    .module('app.core')
    .factory('dataservice', dataservice);

  /* @ngInject */
  function dataservice($http, $location, $q, exception, logger, envService, $resource) {
    var readyPromise;
    var API_ENDPOINT = envService.read('API_ENDPOINT');

    var service = {
      getPhotos: getPhotos,
      about: about,
      contacts: contacts,
      ready: ready
    };

    return service;

    function about() {
      return $resource(API_ENDPOINT + '/about');
    }

    function contacts() {
      return $resource(API_ENDPOINT + '/contacts');
    }


    function getPhotos() {
      return $http.get(API_ENDPOINT + '/lookbook')
        .then(getPhotosComplete)
        .catch(getPhotosFailed);

      function getPhotosComplete(data, status, headers, config) {
        return data.data;
      }

      function getPhotosFailed(e) {
        $location.url('/');
        return exception.catcher('XHR Failed for getCustomer')(e);
      }
    }

    function getCustomers() {
      return $http.get('/api/customers')
        .then(getCustomersComplete)
        .catch(getCustomersFailed);

      function getCustomersComplete(data, status, headers, config) {
        return data.data;
      }

      function getCustomersFailed(e) {
        $location.url('/');
        return exception.catcher('XHR Failed for getCustomers')(e);
      }
    }

    function getReady() {
      if (!readyPromise) {
        // Apps often pre-fetch session data ("prime the app")
        // before showing the first view.
        // This app doesn't need priming but we add a
        // no-op implementation to show how it would work.
        logger.info('Primed the app data');
        readyPromise = $q.when(service);
      }
      return readyPromise;
    }

    function ready(promisesArray) {
      return getReady()
        .then(function () {
          return promisesArray ? $q.all(promisesArray) : readyPromise;
        })
        .catch(exception.catcher('"ready" function failed'));
    }
  }
})();
