/* global toastr:false, moment:false */
(function() {
    'use strict';

    //.constant('API_ENDPOINT', {"url":"http://localhost:3000/api"});
    angular
        .module('app.core')
        .config(function(envServiceProvider) {
            // set the domains and variables for each environment
            envServiceProvider.config({
                domains: {
                    development: ['localhost:3000'],
                    stage: ['stage.soberay.com.ua']
                    // anotherStage: ['domain1', 'domain2'],
                    // anotherStage: ['domain1', 'domain2']
                },
                vars: {
                    development: {
                        API_ENDPOINT: 'http://localhost:3000/api'
                        // staticUrl: '//localhost/static'
                        // antoherCustomVar: 'lorem',
                        // antoherCustomVar: 'ipsum'
                    },
                    stage: {
                        API_ENDPOINT: 'http://stage.soberay.com.ua/api'
                        // staticUrl: '//static.acme.com'
                        // antoherCustomVar: 'lorem',
                        // antoherCustomVar: 'ipsum'
                    }
                    // anotherStage: {
                    //  customVar: 'lorem',
                    //  customVar: 'ipsum'
                    // }
                }
            });

            // run the environment check, so the comprobation is made
            // before controllers and services are built
            envServiceProvider.check();
        });
})();
