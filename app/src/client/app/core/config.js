(function() {
    'use strict';

    var core = angular.module('app.core');

    var config = {
        appErrorPrefix: '[Soberay Error] ', //Configure the exceptionHandler decorator
        appTitle: 'Soberay'
    };

    core.value('config', config);

    core.config(configure);

    configure.$inject = ['$compileProvider', '$logProvider', 'diagnostics', 'exceptionHandlerProvider',
        'routerHelperProvider', 'cfpLoadingBarProvider', '$animateProvider'];

    /* @ngInject */
    function configure($compileProvider, $logProvider, diagnostics, exceptionHandlerProvider,
                       routerHelperProvider, cfpLoadingBarProvider, $animateProvider) {

        diagnostics.enable = false;

        // Remove loading bar spinner
        cfpLoadingBarProvider.includeSpinner = true;
        cfpLoadingBarProvider.includeBar = true;
        // cfpLoadingBarProvider.spinnerTemplate = '<div class="spinner-wrap"><div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>';
        cfpLoadingBarProvider.spinnerTemplate = '<div class="spinner-wrap animated"><div class="spinner"></div></div>';

        // $animateProvider.classNameFilter( /\banimated\b/ );
        $compileProvider.debugInfoEnabled(false);

        // turn debugging off/on (no info or warn)
        if ($logProvider.debugEnabled) {
            $logProvider.debugEnabled(true);
        }
        exceptionHandlerProvider.configure(config.appErrorPrefix);
        configureStateHelper();

        ////////////////

        function configureStateHelper() {
            // var resolveAlways = { /* @ngInject */
            //     ready: function(dataservice) {
            //         return dataservice.ready();
            //     }
            // };
            
            routerHelperProvider.configure({
                docTitle: 'Soberay',
                // resolveAlways: resolveAlways
            });
        }
    }
})();